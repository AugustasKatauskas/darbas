<?php
/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
namespace APP\controller;
class ErrorController 
{
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        // load views

        require APP . 'Views/error/index.php';

    }
}