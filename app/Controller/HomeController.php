<?php

class HomeController 
{  
   //protect formats array
   protected $formats = [
      'CSV' => FormatCSV::class,
      'JSON' => FormatJSON::class,
      'XML' => FormatXML::class
   ];
   // load index view
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        

        require APP . 'Views/home/index.php';

    }

    //create new object in FormatFactory and look for GET method on form where select name was format
    
    /**
     * format
     *
     * @return object
     */
    public function format()
    {

      $formatFactory = new FormatFactory();
      $format = $formatFactory->create($this->getFormat($_GET['format']));

      $people=[
         [
             'first_name' => 'Kiestis',
             'age' => 29,
             'gender' => 'male'
         ],
         [
             'first_name' => 'Vytska',
             'age' => 32,
             'gender' => 'male'
         ],
         [
             'first_name' => 'Karina',
             'age' => 25,
             'gender' => 'female'
         ]
     ];


      $format->selectFormat($people);

   }
   //getting all formats
   /**
    * getFormats
    *
    * @return array
    */
   public function getFormats()
   {  
      return $this->formats;
   }

   //get one choose format and return it
   /**
    * getFormat
    *
    * @param  mixed $format
    *
    * @return array
    */
   public function getFormat($format)
   {
      // TODO add isset check
      if (isset($format)) {
         return $this->formats[$format];
         
      }
      return null;
   }

}