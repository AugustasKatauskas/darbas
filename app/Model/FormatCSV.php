<?php

class FormatCSV implements FormatInterface
{

    //Getting $peoples from FormatModel.php selectFormat function and encode to CSV format
    /**
     * selectFormat
     *
     * @param  mixed[] $peoples
     *
     * @return void
     */
    public function selectFormat($peoples)
    {

        $header = false;

        foreach ($peoples as $people) {
            //If there is no header/keys then output given keys
            if(!$header){ 
                $numOfKeys = count($people);
                $i = 1;
                foreach ($people as $key => $value) {
                    if($i < $numOfKeys){
                        echo $key . ', ';
                    }else{
                        echo $key;
                    }
                    $i++;
                }
                echo "\n";
                $header = true;
            }
            foreach ($people as $key => $value) {
                if($value == end($people)){
                    echo $value;
                }else{
                    echo $value . ', ';
                }

            }
            echo "\n";
        }

        header('Content-Type: HTTP ');
    }
    
  
}