<?php

class FormatFactory
{
    // creating new format

    /**
     * create
     *
     * @param  mixed $format
     *
     * @return array
     */
    public static function create($format)
    {
        return new $format;
    }
   
}