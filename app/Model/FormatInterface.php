<?php

///namespace APP\Model;
//Interfaces allow to create code which specifies which methods a class must implement,
interface FormatInterface
{
    /**
     * selectFormat
     *
     * @param  mixed $peoples
     *
     * @return array
     */
    public function selectFormat($peoples);
}