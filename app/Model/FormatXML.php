<?php

class FormatXML implements FormatInterface
{
    //Getting $peoples from FormatModel.php selectFormat function and encode to XML format

    /**
     * selectFormat
     *
     * @param  mixed[] $peoples
     *
     * @return void
     */
    public function selectFormat($peoples)
    {
        $xml = new SimpleXMLElement('<root/>');
        foreach ($peoples as $people) {
            $newPeople = array_flip($people);
            array_walk_recursive($newPeople, array ($xml, 'addChild'));
        }
        print $xml->asXML();

        header('Content-Type: application/xml; charset=utf-8');
          
          
    }

}