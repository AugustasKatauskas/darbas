<?php

//namespace APP\Model;

class FormatJSON implements FormatInterface
{
    //Getting $peoples from FormatModel.php selectFormat function and encode to JSON format
    /**
     * selectFormat
     *
     * @param  mixed[] $peoples
     *
     * @return void
     */

    public function selectFormat($peoples)
    {  
        $print = json_encode($peoples);
        echo $print;
        header('Content-Type: application/json; charset=utf-8');
        
    }

   
  
}
