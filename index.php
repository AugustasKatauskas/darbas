<?php

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);


// load autoload 
require_once 'bootstrap.php';

// load application config (error reporting etc.)
require APP . 'Config/Config.php';


// start the application
//use APP\Core\Application;
$app = new Application();
