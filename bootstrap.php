<?php

//echo APP . '<br>';
//echo ROOT . '<br>';

spl_autoload_register(function($className) {
    //echo $className . '<br>';
    $dirs = [
        APP . 'Core',
        APP . 'Model',
        APP . 'Controller'
    ];
    
      // Search a class in every directory
    foreach ($dirs as $dir) {
        if (file_exists($dir . '/' . $className . '.php')) {
            // Class file exists, include and return
            //echo 'testas:<br>' . $dir . '/' . $className . '.php';
            include_once ($dir . '/' . $className . '.php');
            return;
        }
    }
});


